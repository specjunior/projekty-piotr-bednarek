﻿namespace Ewidencja
{
    public class ObiektEwidencji
    {
        public int ItemId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Place { get; set; }
        public string Date { get; set; }
        public string AddDate { get; set; }
        public string Files { get; set; }
        public int FakeId { get; set; }
        public System.Windows.Media.SolidColorBrush Brush { get; set; }
        
    }
}
