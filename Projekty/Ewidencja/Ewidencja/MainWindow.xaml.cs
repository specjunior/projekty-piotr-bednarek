﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;

namespace Ewidencja
{
    /// <summary>
    /// Logika interakcji dla klasy MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public static int itemsNumber = 0;
        ListSortDirection _lastDirection = ListSortDirection.Ascending;
        List<ObiektEwidencji> items;
        List<FileButton> buttons ;
        public static string appPath;
        public static string filesDirectories = "Files";//nazwa folderu do którego kopiowane są pliki

        public MainWindow()
        {
            InitializeComponent();
            DownloadData();
            SelectElementByIndex(0);
            //lvUsers.SelectedItem = 2;
            appPath = Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName);
            //warningImage.Visibility=Visibility.Hidden;
        }

        void SaveSplitter()
        {
            Column1.Width = new GridLength(7, GridUnitType.Star);
            Column2.Width = new GridLength(3, GridUnitType.Star);
        }

        void SelectElementByIndex(int index)
        {
            int size = items.Count;
            if (size < 1)
            {
                SetTextSelected("brak danych", "brak danych", "brak danych",null);
            }
            else
            {
                ObiektEwidencji obiekt = (ObiektEwidencji)lvUsers.Items[index];
                SetTextSelected(obiekt.Name, obiekt.Description, obiekt.Place,obiekt);
            }
        }

        void SetTextSelected(string name, string description, string place, ObiektEwidencji obiekt)
        {
            if (name == null || name == "") selectedName.Text = "brak";
            else selectedName.Text = name;

            if (description == null || description == "") selectedDescription.Text = "brak";
            else selectedDescription.Text = description;

            if (place == null || place == "") selectedPlace.Text = "brak";
            else selectedPlace.Text = place;

            if (obiekt.Date != null && obiekt.Date != "")
            {
                int a = (DateTime.Parse(obiekt.Date) - DateTime.Now).Days;
                if (a < 0)
                {
                    warningImageYellow.Visibility = Visibility.Hidden;  //żółty znacznik  niewidoczny
                    warningImageRed.Visibility = Visibility.Visible;    //czerwony znacznik  widoczny
                    GroupBoxZaznaczonyElement.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#FFB7B7"));

                }
                else if (a <= 14)
                {
                    warningImageYellow.Visibility = Visibility.Visible; //żółty znacznik  widoczny
                    warningImageRed.Visibility = Visibility.Hidden; //czerwony znacznik  niewidoczny
                    GroupBoxZaznaczonyElement.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#F9FFB9"));
                }
                else
                {
                    warningImageYellow.Visibility = Visibility.Hidden; //żółty znacznik  niewidoczny
                    warningImageRed.Visibility = Visibility.Hidden; //czerwony znacznik  niewidoczny
                    GroupBoxZaznaczonyElement.Background = Brushes.Transparent;
                }
            }
            else
            {
                warningImageYellow.Visibility = Visibility.Hidden;
                warningImageRed.Visibility = Visibility.Hidden;
                GroupBoxZaznaczonyElement.Background = Brushes.Transparent;
            }

            //przyciski plikow
            List<FileButton> przyciski = FileOperations.ZnajdzPliki(obiekt);
            buttons = przyciski;
            ic.ItemsSource = buttons;

        }

        public void DownloadData()
        {
            items = SqliteDataAccess.LoadPeople();
            itemsNumber = items.Count;
            for (int i = 0; i < itemsNumber; i++)
            {
                items[i].FakeId = i + 1;
                if (items[i].Date != null && items[i].Date != "")
                {
                    int a = (DateTime.Parse(items[i].Date) - DateTime.Now).Days;
                    if (a < 0)
                    {
                        items[i].Brush = Brushes.Red;

                    }
                    else if (a <= 14)
                    {
                        items[i].Brush = Brushes.Yellow;
                    }
                    else
                    {
                        items[i].Brush = Brushes.Transparent;
                    }
                }

            }

            lvUsers.ItemsSource = items;
            itemsNumber = lvUsers.Items.Count;
        }

        private void Sort(string sortBy, ListSortDirection direction)
        {
            ICollectionView dataView =
              CollectionViewSource.GetDefaultView(lvUsers.ItemsSource);

            dataView.SortDescriptions.Clear();
            SortDescription sd = new SortDescription(sortBy, direction);
            dataView.SortDescriptions.Add(sd);
            dataView.Refresh();
        }

        //Funkcje wywoływane z xaml:

        void SortListElements(object sender, RoutedEventArgs e)
        {
            GridViewColumnHeader headerClicked = e.OriginalSource as GridViewColumnHeader;
            ListSortDirection direction;
            if (_lastDirection == ListSortDirection.Descending)
            {
                direction = ListSortDirection.Ascending;
            }
            else
            {
                direction = ListSortDirection.Descending;
            }
            Binding columnBinding = headerClicked.Column.DisplayMemberBinding as Binding;
            string sortBy = columnBinding?.Path.Path ?? headerClicked.Column.Header as string;

            Sort(sortBy, direction);
            _lastDirection = direction;

        }

        //stworz nowy obiekt
        private void CreateNew(object sender, RoutedEventArgs e)
        {
            AddItemWindows subWindow = new AddItemWindows();
            subWindow.ShowDialog();
        }
        private void EditEntry(object sender, RoutedEventArgs e)
        {
            if (lvUsers.Items.Count < 1) return;
            ObiektEwidencji obiekt = (ObiektEwidencji)lvUsers.SelectedItems[0];
            EditElementWindow subWindow = new EditElementWindow(obiekt);
            subWindow.ShowDialog();
        }

        private void RefreshData(object sender, RoutedEventArgs e)
        {
            DownloadData();
        }

        private void QuitApplication(object sender, RoutedEventArgs e)
        {
            System.Windows.Application.Current.Shutdown();
        }

        private void getSelectedItem(object sender, RoutedEventArgs e)
        {
            if (lvUsers.Items.Count < 1) return;
            ObiektEwidencji obiekt = (ObiektEwidencji)lvUsers.SelectedItems[0];
            SetTextSelected(obiekt.Name, obiekt.Description, obiekt.Place,obiekt);
        }

        private void RemoveElement(object sender, RoutedEventArgs e)
        {
            if (lvUsers.Items.Count < 1) return;
            ObiektEwidencji obiekt = (ObiektEwidencji)lvUsers.SelectedItems[0];
            MessageBoxResult result = MessageBox.Show("Czy na pewno chcesz usunąć element \"" + obiekt.Name + "\"?", "Uwaga", MessageBoxButton.YesNoCancel);
            switch (result)
            {
                case MessageBoxResult.Yes:
                    //MessageBox.Show("Tak", "Uwaga");
                    foreach(FileButton file in buttons)
                    {
                        File.Delete(file.FilePath);
                    }
                    SqliteDataAccess.DeletePerson(obiekt);
                    DownloadData();
                    break;
                case MessageBoxResult.No:
                    //MessageBox.Show("Nie", "Uwaga");
                    break;
            }
        }

        //wywoływane w ListView lvUsers w metodzie PreviewKeyDown
        private void Klawisz(object sender, KeyEventArgs e)
        {
            switch(e.Key)
            {
                case Key.Enter:
                    EditEntry(null, null);
                    break;
                case Key.Delete:
                    RemoveElement(null, null);
                    break;
            }
        }
        //otwieranie okna z info o skrótach klawiszowych
        private void ShortcutsWindow(object sender, RoutedEventArgs e)
        {
            //Shortcuts subWindow = new Shortcuts();
            //subWindow.ShowDialog();
            MessageBox.Show("[DELETE]\tusuń zaznaczony element\n[ENTER]\twybierz zaznaczony element", "Skróty klawiszowe", MessageBoxButton.OK, MessageBoxImage.None);
        }
        //okno z info o autorze
        private void OpenAbout(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Piotr Bednarek\n2020\nWersja 1.17.9.20b", "O programie", MessageBoxButton.OK, MessageBoxImage.None);
        }
        private void OtworzPlik(object sender, RoutedEventArgs e)
        {
            if (ic.Items.Count < 1) return;
            FileButton obiekt = (FileButton)ic.SelectedItems[0];
            obiekt.OtworzPlik();
        }
        
    }
    
}
