﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows;

namespace Ewidencja
{
    /// <summary>
    /// Interaction logic for EditElementWindow.xaml
    /// </summary>
    
    public partial class EditElementWindow : Window
    {
        public ObiektEwidencji element = new ObiektEwidencji();
        public double Spacing { get; set; }
        public EditElementWindow(ObiektEwidencji obiekt)
        {
            InitializeComponent();
            OnStart(obiekt);      
        }
        private void OnStart(ObiektEwidencji obiekt)
        {
            element.AddDate = obiekt.AddDate; 
            element.Date = obiekt.Date; 
            element.Description = obiekt.Description; 
            element.Files=obiekt.Files;
            element.ItemId = obiekt.ItemId;
            element.Name = obiekt.Name;
            element.Place = obiekt.Place;
           
            SetElements(obiekt);
        }

        void SetElements(ObiektEwidencji obiekt)
        {
            Nazwa.Text = obiekt.Name;
            Opis.Text = obiekt.Description;
            Miejsce.Text = obiekt.Place;
            if (obiekt.Date != null && obiekt.Date!="") Data.SelectedDate = DateTime.Parse(obiekt.Date);
            if (obiekt.AddDate != null && obiekt.AddDate != "") AddData.SelectedDate = DateTime.Parse(obiekt.AddDate);
            List<FileButton> przyciski = FileOperations.ZnajdzPliki(element);
            ic.ItemsSource = przyciski;
        }

        private void Update(object sender, RoutedEventArgs e)
        {
            DateTime czas = default(DateTime), czasDodania=default(DateTime);

            if (Data.SelectedDate != null) czas = Data.SelectedDate.Value;
            if (AddData.SelectedDate != null) czasDodania = AddData.SelectedDate.Value;
            string name=Nazwa.Text, description=Opis.Text, destination=Miejsce.Text,files = element.Files, date= Data.SelectedDate != null ? czas.ToString("dd.MM.yyyy") : "", addDate = AddData.SelectedDate != null ? czasDodania.ToString("dd.MM.yyyy") : "";

            ObiektEwidencji temp = new ObiektEwidencji();
            temp.ItemId = element.ItemId; temp.Name = name; temp.Description = description; temp.Place = destination;temp.Date = date;temp.AddDate=addDate ; temp.Files=files;
            SqliteDataAccess.UpdatePerson(temp);
            ((MainWindow)System.Windows.Application.Current.MainWindow).DownloadData();
            this.Close();
        }
        private void DodajPlik(object sender, RoutedEventArgs e)
        {
            string temp = FileOperations.AddFile(), file_name = "";
            if (temp == "")
            {
                OdswierzListePlikow();
                return;
            }
            temp = temp.Substring(0, temp.Length-1);
            for (int j = temp.Length - 1; j > 0; j--)
            {
                if (temp[j] == '\\')
                {
                    file_name = temp.Substring(j + 1, temp.Length - j - 1);
                    break;
                }
            }
            if (!Directory.Exists(MainWindow.appPath + "\\" + MainWindow.filesDirectories))
            {
                Directory.CreateDirectory(MainWindow.appPath + "\\" + MainWindow.filesDirectories);
            }
            File.Copy(temp, MainWindow.appPath + "\\" + MainWindow.filesDirectories + "\\" + file_name, true);
            element.Files= string.Concat(element.Files, MainWindow.appPath + "\\" + MainWindow.filesDirectories + "\\" + file_name + ";");
            OdswierzListePlikow();

        }

        void OdswierzListePlikow()
        {
            List<FileButton> przyciski = FileOperations.ZnajdzPliki(element);
            ic.ItemsSource = przyciski;
        }

        private void DeleteFile(object sender, RoutedEventArgs e)
        {
            if (ic.Items.Count < 1) return;
            FileButton obiekt = (FileButton)ic.SelectedItems[0];
            File.Delete(obiekt.FilePath);
            element.Files = element.Files.Replace(obiekt.FilePath + ";", "");
            OdswierzListePlikow();
        }

        private void OtworzPlik(object sender, RoutedEventArgs e)
        {
            if (ic.Items.Count < 1) return;
            FileButton obiekt = (FileButton)ic.SelectedItems[0];
            obiekt.OtworzPlik();
        }

    }
}
