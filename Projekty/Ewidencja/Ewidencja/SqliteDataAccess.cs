﻿using Dapper;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SQLite;
using System.Linq;

namespace Ewidencja
{
    public class SqliteDataAccess
    {
        public static List<ObiektEwidencji> LoadPeople() 
        {
            using (IDbConnection cnn = new SQLiteConnection(LoadConnectionString()))
            {
                var output = cnn.Query<ObiektEwidencji>("select * from Elementy", new DynamicParameters());
                return output.ToList();
            }
        }

        public static void SavePerson(ObiektEwidencji obiekt)
        {
            using (IDbConnection cnn = new SQLiteConnection(LoadConnectionString()))
            {
                cnn.Execute("insert into Elementy (Name, Description, Place, Date, AddDate, Files) values (@Name, @Description, @Place, @Date, @AddDate, @Files)", obiekt);
            }
        }

        public static void UpdatePerson(ObiektEwidencji obiekt)
        {
            using (IDbConnection cnn = new SQLiteConnection(LoadConnectionString()))
            {
                cnn.Execute("update Elementy set Name=@Name, Description=@Description, Place=@Place, Date=@Date, AddDate=@AddDate, Files=@Files where ItemId=@ItemId", obiekt);
            }
        }

        public static void DeletePerson(ObiektEwidencji obiekt)
        {
            using (IDbConnection cnn = new SQLiteConnection(LoadConnectionString()))
            {
                cnn.Execute("delete from Elementy WHERE ItemId=@ItemId", obiekt);
            }
        }

        private static string LoadConnectionString(string id="Default")
        {
            return ConfigurationManager.ConnectionStrings[id].ConnectionString;     
        }

    }
}
