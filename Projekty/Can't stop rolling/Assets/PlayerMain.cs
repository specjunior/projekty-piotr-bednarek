﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using CnControls;
using System;
using UnityEngine.SceneManagement;
using UnityEngine.Audio;
using UnityEditor;

public class PlayerMain : MonoBehaviour
{
	[SerializeField] private Audio audio;
	[SerializeField] private PlayerMotor playerMotor;
	[SerializeField] private Text zrobioneMisje;
	[SerializeField] private Text textM1;
	[SerializeField] private Text textM2;
	[SerializeField] private Text textM3;
	[SerializeField] private GameObject pauseMenu;
	[SerializeField] private GameObject scoreBoard;
	[SerializeField] private GameObject[] Butelki;
	[SerializeField] private GameObject challenge13;
	[SerializeField] private GameObject challenge33;
	[SerializeField] private GameObject challenge23;
	[SerializeField] private GameObject challenge43;
	[SerializeField] private bool isPaused = false, isDead=false;
	private int licznikPrzeskokowHydrant = 0, licznikPrzeskokowDziura = 0, indexStart=0;
	private int kapsleZlote=0, kapsleNiebieskie=0, kapsleCzerwone=0, distance=0;

	private void Start()
	{
		SetStartBottle();
		CheckQuests();
		GetAllStatsKapsle();
	}
	private void Update()
	{
		if (Input.GetButtonDown("Cancel"))
		{
			DoPause();
		}
		else if (Input.GetButtonDown("Cancel"))
		{
			UnPause();
		}

	}

	public int GetKapsle()
	{
		return kapsleZlote;
	}

	public void CollectZloty()
	{
		kapsleZlote++;
		PlayerPrefs.SetInt("pickup", kapsleZlote);
	}
	public void CollectNiebieski()
	{
		kapsleNiebieskie++;
		PlayerPrefs.SetInt("niebieskie", kapsleNiebieskie);
	}
	public void CollectCzerwony()
	{
		kapsleCzerwone++;
		PlayerPrefs.SetInt("czerwone", kapsleCzerwone);
	}

	void SetStartBottle()
	{
		indexStart = PlayerPrefs.GetInt("skin");
		for (int i = 0; i < Butelki.Length; i++)
		{
			if (i == indexStart)
			{
				Butelki[i].SetActive(true);
			}
			else Butelki[i].SetActive(false);
		}
	}

	public void DoPause()
	{
		isPaused = true;		
		Time.timeScale = 0;
		CheckQuests();
		pauseMenu.SetActive(true);
		audio.Stop();
	}

	public void UnPause()
	{
		isPaused = false;
		Time.timeScale = 1;
		pauseMenu.SetActive(false);
		audio.Play();
	}
	void CheckQuests()
    {
		zrobioneMisje.text = PlayerPrefs.GetInt("zrobioneMisje", 0).ToString();
		//misja1
		if (PlayerPrefs.GetInt("Challenge1.1", 0) != 1)
		{
			textM1.text = "Get 750 highscore";
		}
		else if (PlayerPrefs.GetInt("Challenge2.1", 0) != 1 && PlayerPrefs.GetInt("Challenge1.1") == 1)
		{
			textM1.text = "Get 1000 highscore";
		}
		else if (PlayerPrefs.GetInt("Challenge3.1", 0) != 1 && PlayerPrefs.GetInt("Challenge2.1", 0) == 1 && PlayerPrefs.GetInt("Challenge1.1") == 1)
		{
			textM1.text = "Get 1250 highscore";
		}
		else if (PlayerPrefs.GetInt("Challenge4.1", 0) != 1 && PlayerPrefs.GetInt("Challenge3.1", 0) == 1 && PlayerPrefs.GetInt("Challenge2.1", 0) == 1 && PlayerPrefs.GetInt("Challenge1.1") == 1)
		{
			textM1.text = "Get 1500 highscore";
		}
		else
		{
			textM1.text = "All challenges done here";
		}
		//misja2
		if (PlayerPrefs.GetInt("Challenge1.2", 0) != 1)
		{
			textM2.text = "Acquire 2 labels";
		}
		else if (PlayerPrefs.GetInt("Challenge2.2", 0) != 1 && PlayerPrefs.GetInt("Challenge1.2") == 1)
		{
			textM2.text = "Acquire 5 labels";
		}
		else if (PlayerPrefs.GetInt("Challenge3.2", 0) != 1 && PlayerPrefs.GetInt("Challenge2.2", 0) == 1 && PlayerPrefs.GetInt("Challenge1.2") == 1)
		{
			textM2.text = "Acquire 8 labels";
		}
		else if (PlayerPrefs.GetInt("Challenge4.2", 0) != 1 && PlayerPrefs.GetInt("Challenge3.2", 0) == 1 && PlayerPrefs.GetInt("Challenge2.2", 0) == 1 && PlayerPrefs.GetInt("Challenge1.2") == 1)
		{
			textM2.text = "Acquire 10 labels";
		}
		else textM2.text = "All challenges done here";
		//misja3
		if (PlayerPrefs.GetInt("Challenge1.3", 0) != 1)
		{
			textM3.text = "Collect 10 blue caps";
		}
		else if (PlayerPrefs.GetInt("Challenge2.3", 0) != 1 && PlayerPrefs.GetInt("Challenge1.3") == 1)
		{
			textM3.text = "Jump over the hydrant 10 times";
		}
		else if (PlayerPrefs.GetInt("Challenge3.3", 0) != 1 && PlayerPrefs.GetInt("Challenge2.3", 0) == 1 && PlayerPrefs.GetInt("Challenge1.3") == 1)
		{
			textM3.text = "Collect 10 red caps";
		}
		else if (PlayerPrefs.GetInt("Challenge4.3", 0) != 1 && PlayerPrefs.GetInt("Challenge3.3", 0) == 1 && PlayerPrefs.GetInt("Challenge2.3", 0) == 1 && PlayerPrefs.GetInt("Challenge1.3") == 1)
		{
			textM3.text = "Jump over the hole 10 times";
		}
		else textM3.text = "All challenges done here";
		
		
		//challenge2.3
		if (licznikPrzeskokowHydrant >= 10 && PlayerPrefs.GetInt("Challenge2.3", 0) != 1 && (PlayerPrefs.GetInt("Challenge1.3", 0) == 1))
		{
			PlayerPrefs.SetInt("Challenge2.3", 1);
			challenge23.SetActive(true);
			Destroy(challenge23, 4);
			PlayerPrefs.SetInt("zrobioneMisje", PlayerPrefs.GetInt("zrobioneMisje", 0) + 1);
		}
		//challenge4.3
		if (licznikPrzeskokowDziura >= 10 && PlayerPrefs.GetInt("Challenge4.3", 0) != 1 && (PlayerPrefs.GetInt("Challenge3.3", 0) == 1))
		{
			PlayerPrefs.SetInt("Challenge4.3", 1);
			challenge43.SetActive(true);
			Destroy(challenge43, 4);
			PlayerPrefs.SetInt("zrobioneMisje", PlayerPrefs.GetInt("zrobioneMisje", 0) + 1);
		}
	}

	void GetAllStatsKapsle()
	{
		if (PlayerPrefs.HasKey("czerwone"))
			kapsleCzerwone = PlayerPrefs.GetInt("czerwone");
		else
			PlayerPrefs.SetInt("czerwone", 0);

		if (PlayerPrefs.HasKey("niebieskie"))
			kapsleNiebieskie = PlayerPrefs.GetInt("niebieskie");
		else
			PlayerPrefs.SetInt("niebieskie", 0);

		if (PlayerPrefs.HasKey("distance"))
			distance = PlayerPrefs.GetInt("distance");
		else
			PlayerPrefs.SetInt("distance", 0);

		if (PlayerPrefs.HasKey("pickup"))
			kapsleZlote = PlayerPrefs.GetInt("pickup");
		else
			PlayerPrefs.SetInt("pickup", 0);
	}

	void PickUp(Collider other)
	{
		kapsleZlote++;
		PlayerPrefs.SetInt("pickup", kapsleZlote);
		audio.PlaySound("kapsel");
		Destroy(other.gameObject);
	}

	void Przyspieszenie(Collider other)
	{
		kapsleNiebieskie++;
		if ((kapsleNiebieskie >= 10) && PlayerPrefs.GetInt("Challenge1.3", 0) != 1)
		{
			PlayerPrefs.SetInt("Challenge1.3", 1);
			challenge13.SetActive(true);
			Destroy(challenge13, 4);
			PlayerPrefs.SetInt("zrobioneMisje", PlayerPrefs.GetInt("zrobioneMisje", 0) + 1);
		}
		Destroy(other.gameObject);
	}

	void Spowolnienie(Collider other)
	{
		kapsleCzerwone++;
		if ((kapsleCzerwone >= 10) && PlayerPrefs.GetInt("Challenge3.3", 0) != 1)
		{
			PlayerPrefs.SetInt("Challenge3.3", 1);
			challenge33.SetActive(true);
			Destroy(challenge33, 4);
			PlayerPrefs.SetInt("zrobioneMisje", PlayerPrefs.GetInt("zrobioneMisje", 0) + 1);
		}
		Destroy(other.gameObject);
	}

	public void Death()
	{
		//scoreText.text = ((int)score).ToString();
		isDead = true;
		audio.PlaySound("pekniecie");
		audio.Stop();
		Butelki[indexStart].SetActive(false);
		GetComponent<Score>().OnDeath();
		Time.timeScale = 0;
		scoreBoard.SetActive(true);
		//SceneManager.LoadScene("Scene");
	}
	public void respawn()
	{
		Time.timeScale = 1;
		SceneManager.GetActiveScene();
	}
	public void home()
	{
		Time.timeScale = 1;
		SceneManager.LoadScene("Main Menu");
	}
	public void shop()
	{
		Time.timeScale = 1;
		SceneManager.LoadScene("Sklep");
	}

	private void OnTriggerEnter(Collider other)
	{
		if(other.tag == "nad_hydrantem")
		{
			licznikPrzeskokowHydrant++;
		}
		else if(other.tag =="nad_dziura")
		{
			licznikPrzeskokowDziura++;
		}

		if(other.tag=="Enemy")
		{
			Death();
			Debug.Log("Enemy");
		}else if(other.tag=="Pick up")
		{
			PickUp(other);
		}else if(other.tag == "przyspieszenie")
		{
			Przyspieszenie(other);
		}
		else if(other.tag == "spowolnienie")
		{
			Spowolnienie(other);
		}
	}

}
