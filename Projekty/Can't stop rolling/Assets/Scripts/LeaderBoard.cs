﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LeaderBoard : MonoBehaviour {

    public int highScore;
	public int niebieskie;
	public int czerwone;
	public int distance;
    public Text highScoreText;
	public Text niebieskie1;
	public Text czerwone1;
	public Text distance1;
    //public GameObject tablicaWyn;

    void Start ()
    {
        //tablicaWyn.SetActive(false);
        highScore = PlayerPrefs.GetInt("HighScore1");
    }

   /* public void pokazTablice()
    {
        tablicaWyn.SetActive(true);

    }

    public void schowajTablice()
    {
        tablicaWyn.SetActive(false);
    }*/

    void Update ()
    {
        highScoreText.text = highScore.ToString();

		niebieskie = PlayerPrefs.GetInt("niebieskie");
		niebieskie1.text = "x " + niebieskie.ToString();

		czerwone = PlayerPrefs.GetInt("czerwone");
		czerwone1.text = "x " + czerwone.ToString();

		distance = PlayerPrefs.GetInt("distance");
		distance1.text =distance.ToString();
	}
}
