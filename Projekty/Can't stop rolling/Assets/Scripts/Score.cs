﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
public class Score : MonoBehaviour {

	public float score = 0.0f;

	public int difficultyLevel = 4;
	private int maxDifficultyLevel = 25;
	private int scoreToNextLevel = 300;
	
	private bool isDead = false;

    public Text iloscKapsli;
    public Text scoreText;
    public Text highScore;
    public Text scoreText_deathMenu;
    public Text highScore_deathMenu;
    public GameObject challenge11;
    public GameObject challenge21;
    public GameObject challenge31;
    public GameObject challenge41;
	public int klucze;
	public PlayerMotor playerMotor;
    public PlayerMain playerMain;


    private void Start()
    {
        challenge11.SetActive(false);
        challenge21.SetActive(false);
        challenge31.SetActive(false);
        challenge41.SetActive(false);

		if (PlayerPrefs.HasKey ("maszyna"))
			klucze = PlayerPrefs.GetInt ("maszyna");
		else
			PlayerPrefs.SetInt("maszyna", 0);
    }

    void Update ()
    {
		
		if(!isDead)
        {
            iloscKapsli.text = playerMain.GetKapsle().ToString();
            if (score >= scoreToNextLevel) 
			    LevelUp();
		
			float speed = playerMotor.GetSpeed();
			score += Time.deltaTime * speed;
			scoreText.text = ((int)score).ToString();
            // challenge1.1
            if ((score >= 750) && (PlayerPrefs.GetInt("Challenge1.1", 0) != 1))
            {
                PlayerPrefs.SetInt("Challenge1.1", 1);
                challenge11.SetActive(true);
                Destroy(challenge11, 4);
                PlayerPrefs.SetInt("zrobioneMisje", PlayerPrefs.GetInt("zrobioneMisje", 0) + 1);
				PlayerPrefs.SetInt ("maszyna", +1);
            }
            // challenge2.1
            if ((score >= 1000) && (PlayerPrefs.GetInt("Challenge2.1", 0) != 1) && (PlayerPrefs.GetInt("Challenge1.1", 0) == 1))
            {
                PlayerPrefs.SetInt("Challenge2.1", 1);
                challenge21.SetActive(true);
                Destroy(challenge21, 4);
                PlayerPrefs.SetInt("zrobioneMisje", PlayerPrefs.GetInt("zrobioneMisje", 0) + 1);
				PlayerPrefs.SetInt ("maszyna", +1);
            }
            // challenge3.1
            if ((score >= 1250) && (PlayerPrefs.GetInt("Challenge3.1", 0) != 1) && (PlayerPrefs.GetInt("Challenge2.1", 0) == 1))
            {
                PlayerPrefs.SetInt("Challenge3.1", 1);
                challenge31.SetActive(true);
                Destroy(challenge31, 4);
                PlayerPrefs.SetInt("zrobioneMisje", PlayerPrefs.GetInt("zrobioneMisje", 0) + 1);
				PlayerPrefs.SetInt ("maszyna", +1);
            }
            // challenge4.1
            if ((score >= 1500) && (PlayerPrefs.GetInt("Challenge4.1", 0) != 1) && (PlayerPrefs.GetInt("Challenge3.1", 0) == 1))
            {
                PlayerPrefs.SetInt("Challenge4.1", 1);
                challenge41.SetActive(true);
                Destroy(challenge41, 4);
                PlayerPrefs.SetInt("zrobioneMisje", PlayerPrefs.GetInt("zrobioneMisje", 0) + 1);
				PlayerPrefs.SetInt ("maszyna", +1);
            }
            if (PlayerPrefs.GetInt("HighScore1") > score)
            {
                highScore.text = PlayerPrefs.GetInt("HighScore1").ToString();
            }
            else if (PlayerPrefs.GetInt("HighScore1") < score)
            {
                highScore.text = ((int)score).ToString();
            }
        }
    

    }
	
	public void LevelUp() {
		if(difficultyLevel == maxDifficultyLevel)
			return;
		
		scoreToNextLevel *= 2;
		difficultyLevel++;
		
		//GetComponent<PlayerMotor>().SetSpeed(difficultyLevel);
		
	}
	
	public void OnDeath() {
		isDead = true;
        if (PlayerPrefs.GetInt("HighScore1") < score)
        {
            PlayerPrefs.SetInt("HighScore1", (int)score);
        }
        UstawPunktacjeNaMenuPoSmierci();
    }

    private void UstawPunktacjeNaMenuPoSmierci()
    { 
        scoreText_deathMenu.text = Convert.ToInt32(playerMain.GetKapsle()).ToString();
        highScore_deathMenu.text = score.ToString();
    }

    public void Restart()
    {
        score = 0;
    }
}
