﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class kasa_sklep : MonoBehaviour {

	public Text kasa;
	public int count;
	// Use this for initialization
	void Start () {
		if (PlayerPrefs.HasKey("pickup"))
			count = PlayerPrefs.GetInt("pickup");
        else
        {
			PlayerPrefs.SetInt("pickup", 0);
			count = 0;
		}


        kasa.text = count.ToString();
    }
	
	// Update is called once per frame
	
}
