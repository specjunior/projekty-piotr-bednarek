﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using CnControls;
using System;
using UnityEngine.SceneManagement;
using UnityEngine.Audio;
using UnityEngine.XR;

public class PlayerMotor : MonoBehaviour
{
    [SerializeField] private bool isMove = false;
    [SerializeField] private float speed = 9f;
    [SerializeField] private float jumpForce = 15.0F;
    [SerializeField] private float gravity = 20.0F;
    private float verticalVelocity = 0.0f;
    [SerializeField] private GameObject butelki;
    [SerializeField] private Rigidbody rb;
    [SerializeField] private CharacterController characterController;
    [SerializeField] private Vector3 moveDirection = Vector3.zero;

    private void Start()
    {
        StartRolling();
    }
    private void Update()
    {
        if(isMove)
        {
            moveDirection.y -= gravity * Time.deltaTime;
            if (characterController.isGrounded && CnInputManager.GetButtonDown("Jump"))
            {
                moveDirection.y = jumpForce;
            }
            if (characterController.isGrounded)
            {
                verticalVelocity = 0.5f;
            }
            else
            {
                verticalVelocity += 12.0f * Time.deltaTime;
            }

            moveDirection.x = CnInputManager.GetAxisRaw("Horizontal") * 5;
            moveDirection.z = speed;
            characterController.Move(moveDirection * Time.deltaTime);
        }
    }
    void StartRolling()
    {
        isMove = true;
    }

    public float GetSpeed()
    {
        return speed;
    }

    public void SpeedUP(float modifier)
    {        
         speed =+ modifier;        
    }

    public void Stop()
    {
        isMove = false;
    }
}