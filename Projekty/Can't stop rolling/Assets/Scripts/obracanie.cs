﻿using UnityEngine;
using System.Collections;
using UnityEditor;

public class obracanie : MonoBehaviour {

	[Tooltip("Jeżeli puste, to bierze prędkość obrotu z wartości w skrypcie")]
	[SerializeField] private PlayerMotor playerMotor;
	[SerializeField] private float speed=5;
	[SerializeField] private Vector3 rotateVector;
	void Update () 
	{
		if(playerMotor!=null) speed = playerMotor.GetSpeed() * 10;
		transform.Rotate (rotateVector * Time.deltaTime * speed);
	}
}