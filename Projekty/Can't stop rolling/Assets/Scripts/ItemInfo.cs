﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ItemInfo : MonoBehaviour {
    [Header("Default values")]
    public int Id=0;
    public int price=0;
    public string name;
    [SerializeField]
    private string description;
    public bool isSpecial=false;
    public bool isBought = false;
    public Sprite image;

    private void Awake()
    {
        if (PlayerPrefs.HasKey(name))
        {
            isBought = Convert.ToBoolean(PlayerPrefs.GetInt(name));        
        }
        else
            PlayerPrefs.SetInt(name, Convert.ToInt32(isBought));
        if (isBought) price = 0;
    }

    public string GetDescriprion()
    {
        return description;
    }
}
