﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class obracanie_maszyna : MonoBehaviour {
	[HideInInspector] public bool inMainMenu = true;					//If true, pause button disabled in main menu (Cancel in input manager, default escape key)
	[HideInInspector] public bool inSklep = true;	
	 public Animator animColorFade; 					//Reference to animator which will fade to and from black when starting game.
	public Animator animMenuAlpha;					//Reference to animator that will fade out alpha of MenuPanel canvas group
	public AnimationClip fadeColorAnimationClip;		//Animation clip fading to color (black default) when changing scenes
	 public AnimationClip fadeAlphaAnimationClip;

	public Animator ani;
	public Animator właz1;
	public Animator właz2;
	public int franek;
	public int wacek;
	public int liczba1;
	public int liczba2;
	public int liczba3;
	public int liczba4;
	public GameObject kruk;
	public GameObject napis2;
	public GameObject napis3;
	public GameObject napis4;

	public GameObject orange;
	public GameObject wine;
	public GameObject groszek;
	public GameObject whiskey;
	public AudioSource zrodloDzwieku;
	public AudioClip odglos;
	public int maszynka;

	public GameObject przycisk1;
	// Use this for initialization

	//public Animation anim;

	void Start (){
		ani.enabled = false;
		właz1.enabled = false;
		właz2.enabled = false;

		/*if (PlayerPrefs.HasKey ("liczba1"))
			liczba1 = PlayerPrefs.GetInt ("liczba1");
		else
			PlayerPrefs.SetInt("liczba1", 0);
		if (PlayerPrefs.HasKey ("liczba2"))
			liczba2 = PlayerPrefs.GetInt ("liczba2");
		else
			PlayerPrefs.SetInt("liczba2", 0);
		if (PlayerPrefs.HasKey ("liczba3"))
			liczba3 = PlayerPrefs.GetInt ("liczba3");
		else
			PlayerPrefs.SetInt("liczba3", 0);
		if (PlayerPrefs.HasKey ("liczba4"))
			liczba4 = PlayerPrefs.GetInt ("liczba4");
		else
			PlayerPrefs.SetInt("liczba4", 0);*/
	}

	public void Losowanie(){

		maszynka = PlayerPrefs.GetInt ("maszyna");
			PlayerPrefs.SetInt("maszyna", maszynka-1);

			Debug.Log ("czas minął");
			franek = Random.Range(1,4);

			if (franek == 1) {
				ani.SetFloat ("animacja", 0);
				Debug.Log ("wylosowano orange");
				ani.enabled = true;

			if (PlayerPrefs.HasKey ("orange"))
				PlayerPrefs.SetInt ("orange", 1);
			else
				PlayerPrefs.SetInt("orange", 1);

					kruk.SetActive (true);
					napis2.SetActive (false);
					napis3.SetActive (false);
					napis4.SetActive (false);

				orange.SetActive (true);
				wine.SetActive (false);
				groszek.SetActive (false);
				whiskey.SetActive (false);
			}
			if (franek == 2) {
				ani.SetFloat ("animacja", 2);
				Debug.Log ("wylosowano wine");
				ani.enabled = true;

			if (PlayerPrefs.HasKey ("wine"))
				PlayerPrefs.SetInt ("wine", 1);
			else
				PlayerPrefs.SetInt("wine", 1);

					kruk.SetActive (false);
					napis2.SetActive (true);
					napis3.SetActive (false);
					napis4.SetActive (false);
			        orange.SetActive (false);
			        wine.SetActive (true);
			        groszek.SetActive (false);
			        whiskey.SetActive (false);
			}

			if (franek == 3) {
				ani.SetFloat ("animacja", 0);
				Debug.Log ("wylosowano groszek");
				ani.enabled = true;

			if (PlayerPrefs.HasKey ("groszek"))
				PlayerPrefs.SetInt ("groszek", 1);
			else
				PlayerPrefs.SetInt("groszek", 1);

					kruk.SetActive (false);
					napis2.SetActive (false);
					napis3.SetActive (true);
					napis4.SetActive (false);
					orange.SetActive (false);
					wine.SetActive (false);
					groszek.SetActive (true);
					whiskey.SetActive (false);
			}
		    if (franek == 4) {
			ani.SetFloat ("animacja", 2);
			Debug.Log ("wylosowano whiskey");
			ani.enabled = true;

			if (PlayerPrefs.HasKey ("whiskey"))
				PlayerPrefs.SetInt ("whiskey", 1);
			else
				PlayerPrefs.SetInt("whiskey", 1);

				kruk.SetActive (false);
				napis2.SetActive (false);
				napis3.SetActive (false);
				napis4.SetActive (true);
			    orange.SetActive (false);
			    wine.SetActive (false);
			    groszek.SetActive (false);
			    whiskey.SetActive (true);
		}
	}
	public void włazy(){
		właz1.enabled = true;
		właz2.enabled = true;
		zrodloDzwieku.PlayOneShot(odglos);
	}

	public void przycisk(){
		przycisk1.SetActive (false);
	}
	public void Menu(){
		Invoke ("LoadDelayed", fadeColorAnimationClip.length * .5f);

		//Set the trigger of Animator 6animColorFade to start transition to the FadeToOpaque state.
		animColorFade.SetTrigger ("fade");
	}

	public void LoadDelayed()
	{
		//Pause button now works if escape is pressed since we are no longer in Main menu.
		inMainMenu = false;
		inSklep = false;
		//Hide the main menu UI element
		//Load the selected scene, by scene index number in build settings
		SceneManager.LoadScene ("Main Menu");
	}
	// Update is called once per frame

}