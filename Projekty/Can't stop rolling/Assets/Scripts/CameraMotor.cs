﻿using UnityEngine;
using System.Collections;


public class CameraMotor : MonoBehaviour {

	public Transform lookAt;
	public Vector3 startOffset;
	public Vector3 moveVector;
	
	private float transition = 0.0f;
	private float animationDuration = 1.0f;
	private Vector3 animationOffset = new Vector3(0,2,4);
	private bool canMove = true;
	
	// Use this for initialization
	void Start () {
		lookAt = GameObject.FindWithTag("Player").transform;
		startOffset = transform.position - lookAt.position;
		//startOffset = transform.position;
	}
	public void StartMove()
	{
		canMove = true;
	}
	public void StopMove()
	{
		canMove = false;
	}

	// Update is called once per frame
	void Update () {
		if (canMove)
		{
			moveVector = lookAt.position + startOffset;

			moveVector.x = -1.6f;
			moveVector.y = Mathf.Clamp(moveVector.y, 0, 10);

			if (transition > 1.0f)
			{
				transform.position = moveVector;
			}
			else
			{
				transform.position = Vector3.Lerp(moveVector + animationOffset, moveVector, transition);
				transition += Time.deltaTime * 1 / animationDuration;
			}
		}
	}
}