﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Audio : MonoBehaviour
{
	[SerializeField] private AudioSource audioSource;
	[SerializeField] private AudioSource audioSourceLoop;
	[SerializeField] private AudioClip menu1;
	[SerializeField] private AudioClip menu2;
	[SerializeField] private AudioClip kapsel;
	[SerializeField] private AudioClip pekniecie;
	[SerializeField] private AudioClip odglos5;
	[SerializeField] private AudioClip odglos6;
	[SerializeField] private AudioClip odglos7;

	public void PlaySound(string name)
	{
		switch(name)
		{
			case "menu1":
				audioSource.PlayOneShot(menu1);
				break;
			case "menu2":
				audioSource.PlayOneShot(menu2);
				break;
			case "kapsel":
				audioSource.PlayOneShot(kapsel);
				break;
			case "pekniecie":
				audioSource.PlayOneShot(pekniecie);
				break;
		}
	}
	public void Stop()
	{
		audioSourceLoop.Stop();
	}

	public void Play()
	{
		audioSourceLoop.Play();
	}
}
