﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadTargetSceneButton : MonoBehaviour {

   // public Animator animColorFade;                  //Reference to animator which will fade to and from black when starting game.     ut alpha of MenuPanel canvas group
   // public AnimationClip fadeColorAnimationClip;

	public void LoadSceneNum(int num){

		Time.timeScale = 1;
		if (num < 0 || num >= SceneManager.sceneCountInBuildSettings) {
			Debug.LogWarning ("Can't load scene num "+ num +", SceneManager only has " + SceneManager.sceneCountInBuildSettings + " scenes in BuildSettings!");
			return;
		}
       // Invoke("test", fadeColorAnimationClip.length * .5f);
       // animColorFade.SetTrigger("fade");

        LoadingScreenManager.LoadScene (num);

	}
}
