﻿using UnityEngine;
using System.Collections;
using UnityEngine.Audio;
using UnityEngine.UI;

public class SetAudioLevels : MonoBehaviour {

	public AudioMixer mainMixer;                    //Used to hold a reference to the AudioMixer mainMixer
	[SerializeField] private Slider musicSlider;
	[SerializeField] private Slider sfxSlider;
	float musiclvl=0, sfxlvl=0;

	private void Start()
	{
		Debug.Log("XD");
		if (PlayerPrefs.HasKey("musicVol"))
		{
			musiclvl = PlayerPrefs.GetFloat("musicVol");
			mainMixer.SetFloat("musicVol", Mathf.Log10(musiclvl) * 20);
			musicSlider.value = musiclvl;
		}
		else
		{
			PlayerPrefs.SetFloat("musicVol", musicSlider.value);
			musiclvl = musicSlider.value;
		}

		if (PlayerPrefs.HasKey("sfxVol"))
		{
			sfxlvl = PlayerPrefs.GetFloat("sfxVol");
			mainMixer.SetFloat("sfxVol", Mathf.Log10(sfxlvl) * 20);
			sfxSlider.value = sfxlvl;
		}
		else
		{
			PlayerPrefs.SetFloat("sfxVol", sfxSlider.value);
			sfxlvl = sfxSlider.value;
		}
	}

	//Call this function and pass in the float parameter musicLvl to set the volume of the AudioMixerGroup Music in mainMixer
	public void SetMusicLevel(float musicLvl)
	{
		mainMixer.SetFloat("musicVol", Mathf.Log10(musicLvl) * 20);
		musiclvl = musicLvl;
		PlayerPrefs.SetFloat("musicVol", musiclvl);
	}

	//Call this function and pass in the float parameter sfxLevel to set the volume of the AudioMixerGroup SoundFx in mainMixer
	public void SetSfxLevel(float sfxLevel)
	{
		mainMixer.SetFloat("sfxVol", Mathf.Log10(sfxLevel) * 20);
		sfxlvl = sfxLevel;
		PlayerPrefs.SetFloat("sfxVol", sfxlvl);
	}
}
