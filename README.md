Archiwum projektów, które powstały z moim udziałem

-Ewidencja: 
Projekt prostego programu, który ma usprawnić ewidencjonowanie sprzętu oraz oprogramowania znajdującego się w firmie. Pozwala na wprowadzanie nowych urządzeń, bądź dowolnej innej rzeczy, do rejestru oraz daje możliwość załączenia do rekordu np: dokumentacji lub instrukcji w zorganizowany sposób.

- Patopiraci:
Prototyp pierwszoosobowej gry, w której gracz wciela się w postać dostawcy wartościowych przedmiotów do zatopionych, post apokaliptycznych miast. Jako dostawcy pływamy pomiędzy miastami dostarczając oscypki i inne przedmioty pierwszej potrzeby. Gracz może ulepszać swój statek dodając sieci, które potrafią łapać pływające przedmioty lub systemy obrony. Systemy obrony są potrzebne do obrony przed pato piratami, którzy pływają po morzach i oceanach czychając na okazję do ograbienia gracza

-Can't stop rolling:
Gra stworzona na telefony z systemem Android. W grze sterujemy butelką toczącą się po chodniku wzdłuż ulic miasta omijając przy tym różnorakie przeszkody. Naszym zadaniem jest zbieranie złotych kapsli które są w grze walutą, za którą możemy kupić nowe skórki naszej butelki. W grze są do zebrania power-upy dające chwilowe przyspieszenie lub spowolnienie.

-Crypto Fighter:
Gra stworzona na urządzenia z systemem Android wspierające technologię Google AR Core. Naszym głównym zadaniem jest obrona koparki bitcoinów przed agresywnymi robotami chcącymi ją zniszczyć. Dzięki wykopanej krypto walucie możemy kupować automatyczne działka, które pomagają obronić naszą koparkę. Ilość przychodów jest zależna od tego, w jakim momencie znajduje się symulowana giełda. Poziom trudności gry jest dopasowywany przez gracza, ponieważ im większe pole do gry zostanie zeskanowane, tym liczniejsi będą wrogowie. Co jakiś czas w trakcie rozgrywki przylatuje satelita, który musi być jak najszybciej zniszczony, ponieważ jego atak całkowicie niszczy naszą koparkę.